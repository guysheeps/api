package kr.guysheep.api.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.guysheep.api.LoadContext;
import kr.guysheep.api.example.domain.Example;
import kr.guysheep.api.example.repository.ExampleRepo;
import kr.guysheep.api.example.service.ExampleService;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ExampleControllerTest extends LoadContext {
  public static final String API_V1_EXAMPLE = "/api/v1/example";
  @Autowired
  private MockMvc mvc;

  @Autowired
  private ExampleService service;

  @Autowired
  private ExampleRepo repo;

  @Autowired
  private ObjectMapper mapper;

  @Test
  @Transactional
  public void 생성() throws Exception {
    mvc.perform(post(API_V1_EXAMPLE)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(mapper.writeValueAsBytes(Example.builder()
                    .name("test")
                    .description("description")
                    .build()))
    )
            .andDo(print())
            .andExpect(jsonPath("$.name").value("test"))
            .andExpect(jsonPath("$.description").value("description"))
            .andExpect(status().isOk());
  }

  @Test
  @Transactional
  public void 조회() throws Exception {
    // given
    // not compliance 생성();
    Example example = service.create(Example.builder()
            .name("name")
            .description("description")
            .build());

    mvc.perform(get(API_V1_EXAMPLE + "/{exampleId}", example.getExampleId()))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(example.getName()))
            .andExpect(jsonPath("$.exampleId").value(example.getExampleId()))
    ;
  }
}