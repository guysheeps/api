package kr.guysheep.api;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("intg,embedded")
@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
public abstract class LoadContext {

  static {
    System.getProperties().setProperty("spring.config.name", "boot");
  }
}
