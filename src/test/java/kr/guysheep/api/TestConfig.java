package kr.guysheep.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class TestConfig {
  @Autowired
  private WebApplicationContext ctx;

  @Profile("intg")
  @Bean
  public MockMvc mvc() {
    return MockMvcBuilders.webAppContextSetup(ctx).build();
  }
}
