set mode MySQL;

DROP TABLE IF EXISTS `Example`;
CREATE TABLE IF NOT EXISTS `Example` (
  `ExampleId` BIGINT NOT NULL AUTO_INCREMENT COMMENT '예제 번호',
  `Name` VARCHAR(25) NOT NULL COMMENT '예제 이름',
  `Description` VARCHAR(255) NOT NULL COMMENT '예제 설명',
  PRIMARY KEY (`ExampleId`),
  UNIQUE `UX_Example__Name` (`Name`)
);