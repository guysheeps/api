package kr.guysheep.api.example.service

import kr.guysheep.api.example.converter.ExampleConverter
import kr.guysheep.api.example.domain.Example
import kr.guysheep.api.example.entity.ExampleEntity
import kr.guysheep.api.example.repository.ExampleRepo
import kr.guysheep.exception.CannotPersistException
import org.mockito.MockitoAnnotations
import org.mockito.internal.util.reflection.Whitebox
import spock.lang.Specification
import spock.lang.Subject

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.*

class ExampleServiceTest extends Specification {
    @Subject
    ExampleService sut = new ExampleService();
    def repo = mock(ExampleRepo.class);

    def setup() {
        MockitoAnnotations.initMocks(this);

        Whitebox.setInternalState(sut, "repo", repo);
        Whitebox.setInternalState(sut, "converter", new ExampleConverter());

    }

    def cleanup() {
    }

    def '예제 데이터 입력'() {
        given:
        def example = Example.builder()
                .name("test")
                .description("description")
                .build();

        when(repo.save(any(ExampleEntity.class))).thenReturn(ExampleEntity.builder()
                .exampleId(1)
                .name("test")
                .description("description")
                .build());
        when:
        def created = sut.create(example);
        verify(repo, times(1)).save(any(ExampleEntity.class));

        then:

        created.exampleId == 1
        created.name.equals("test")
        created.description.equals("description")
    }

    def '데이터 입력에서 null이 반환되는 경우'() {
        given:
        def example = Example.builder()
                .name("test")
                .description("description")
                .build();
        when(repo.save(any(ExampleEntity.class))).thenReturn(null);

        when:
        def created = sut.create(example);
        verify(repo, times(1)).save(any(ExampleEntity.class));

        then:
        thrown CannotPersistException
    }

}
