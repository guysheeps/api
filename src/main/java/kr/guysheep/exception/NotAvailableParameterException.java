package kr.guysheep.exception;

/**
 * @Description
 * @Since 2018. 03. 29.
 * @Version 1.0
 * @COPYRIGHT © Guysheep ALL RIGHTS RESERVED.
 */
public class NotAvailableParameterException extends RuntimeException {
  public NotAvailableParameterException() {
  }

  public NotAvailableParameterException(String message) {
    super(message);
  }

  public NotAvailableParameterException(String message, Throwable cause) {
    super(message, cause);
  }

  public NotAvailableParameterException(Throwable cause) {
    super(cause);
  }

  public NotAvailableParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
