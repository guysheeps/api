package kr.guysheep.exception;

/**
 * @Description
 * @Since 2018. 03. 30.
 * @Version 1.0
 * @COPYRIGHT © WADIZ ALL RIGHTS RESERVED.
 */
public class CannotPersistException extends RuntimeException {
  public CannotPersistException() {
  }

  public CannotPersistException(String message) {
    super(message);
  }

  public CannotPersistException(String message, Throwable cause) {
    super(message, cause);
  }

  public CannotPersistException(Throwable cause) {
    super(cause);
  }

  public CannotPersistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
