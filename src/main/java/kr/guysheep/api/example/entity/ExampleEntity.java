package kr.guysheep.api.example.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@ToString
@Entity
@NoArgsConstructor
public class ExampleEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long exampleId;

  private String name;

  private String description;


  /**
   * 데이터 엔티티를 도메인 엔티티로 변환하기 위한 모든 필드 기준의 생성자 구성
   *
   * @param exampleId
   * @param name
   * @param description
   */
  @Builder
  public ExampleEntity(Long exampleId, String name, String description) {
    this.exampleId = exampleId;
    this.name = name;
    this.description = description;
  }

}
