package kr.guysheep.api.example;

import kr.guysheep.api.example.domain.Example;
import kr.guysheep.api.example.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/example")
@RestController
public class ExampleController {
  @Autowired
  private ExampleService service;

  @PostMapping("")
  public Example post(@RequestBody Example example) {
    return service.create(example);
  }

  @GetMapping("/{exampleId}")
  public Example get(@PathVariable Long exampleId) {
    return service.get(new Example(exampleId, null, null));
  }

  @PutMapping("/{exampleId}")
  public Example put(@PathVariable Long exampleId, @RequestBody Example example) {
    return service.update(exampleId, example);
  }

  @DeleteMapping("/{exampleId}")
  public void delete(@PathVariable Long exampeId) {
    service.delete(exampeId);
  }
}
