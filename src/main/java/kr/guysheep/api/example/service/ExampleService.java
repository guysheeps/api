package kr.guysheep.api.example.service;

import kr.guysheep.api.example.converter.ExampleConverter;
import kr.guysheep.api.example.domain.Example;
import kr.guysheep.api.example.entity.ExampleEntity;
import kr.guysheep.api.example.repository.ExampleRepo;
import kr.guysheep.exception.CannotPersistException;
import kr.guysheep.exception.NotAvailableParameterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExampleService {
  @Autowired
  private ExampleRepo repo;

  @Autowired
  private ExampleConverter converter;

  public Example create(Example example) {
    ExampleEntity entity = converter.convert(example);
    ExampleEntity saved = repo.save(entity);
    if(saved == null) {
      throw new CannotPersistException("failed persists");
    }
    return converter.convert(saved);
  }

  public Example get(Example example) {
    return converter.convert(repo.findOne(example.getExampleId()));
  }

  public Example update(Long id, Example example) {
    ExampleEntity entity = converter.convert(id, example);
    ExampleEntity save = repo.save(entity);
    if (save == null) {
      throw new NotAvailableParameterException("Cannot update example");
    }
    return converter.convert(repo.findOne(id));
  }

  public void delete(Long id) {
    repo.delete(id);
  }

}
