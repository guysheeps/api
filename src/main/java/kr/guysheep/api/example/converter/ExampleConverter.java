package kr.guysheep.api.example.converter;

import kr.guysheep.api.example.domain.Example;
import kr.guysheep.api.example.entity.ExampleEntity;
import org.springframework.stereotype.Component;

@Component
public class ExampleConverter {
  public ExampleEntity convert(Example example) {
    return new ExampleEntity(example.getExampleId(), example.getName(), example.getDescription());
  }

  public Example convert(ExampleEntity entity) {
    return new Example(entity.getExampleId(), entity.getName(), entity.getDescription());
  }

  public ExampleEntity convert(Long id, Example example) {
    return new ExampleEntity(id, example.getName(), example.getDescription());
  }
}
