package kr.guysheep.api.example.repository;

import kr.guysheep.api.example.entity.ExampleEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExampleRepo extends PagingAndSortingRepository<ExampleEntity, Long> {
}
