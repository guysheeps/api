package kr.guysheep.api.example.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
public class Example {
  private Long exampleId;
  private String name;
  private String description;

  @Builder
  public Example(Long exampleId, String name, String description) {
    this.exampleId = exampleId;
    this.name = name;
    this.description = description;
  }
}
